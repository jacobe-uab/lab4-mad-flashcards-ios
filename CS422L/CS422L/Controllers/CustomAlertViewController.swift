//
//  CustomAlertViewController.swift
//  CS422L
//
//  Created by Jacob Ellis on 2/15/21.
//

import UIKit

class CustomAlertViewController: UIViewController {
    
    var parentVC: FlashCardSetDetailViewController!
    var cardIndex: Int = 0

    @IBOutlet var termField: UITextField!
    @IBOutlet var definitionField: UITextField!
    @IBOutlet var alertView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        termField.text = parentVC.cards[cardIndex].term
        definitionField.text = parentVC.cards[cardIndex].definition
        
        alertView.layer.cornerRadius = 13.0
    }

    @IBAction func saveCard(_ sender: Any) {
        parentVC.cards[cardIndex].term = termField.text ?? ""
        parentVC.cards[cardIndex].definition = definitionField.text ?? ""
        parentVC.tableView.reloadData()
        self.dismiss(animated: true)
    }
    
    @IBAction func deleteCard(_ sender: Any) {
        parentVC.cards.remove(at: cardIndex)
        parentVC.tableView.reloadData()
        self.dismiss(animated: true)
    }
}
