//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Jacob Ellis on 2/15/21.
//

import UIKit

class StudySetViewController: UIViewController {
    
    @IBOutlet var cardView: UIView!
    @IBOutlet var missedButton: UIButton!
    @IBOutlet var missedCountText: UILabel!
    @IBOutlet var skipButton: UIButton!
    @IBOutlet var correctButton: UIButton!
    @IBOutlet var exitButton: UIButton!
    @IBOutlet var correctCountText: UILabel!
    @IBOutlet var termText: UILabel!
    @IBOutlet var definitionText: UILabel!
    @IBOutlet var completedCountText: UILabel!
    
    var cards: [Flashcard] = []
    var startCount: Int = 0
    var missedCards: [Flashcard] = []
    var cardCleared: Bool = false
    var correct: Int = 0;
    var complete: Int = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startCount = cards.count
        doFormat()
        updateCounts()
        updateCard()
        
        let directions: [UISwipeGestureRecognizer.Direction] = [.left, .right, .up]
        
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(respondToCardSwipe))
            gesture.direction = direction
            cardView.addGestureRecognizer(gesture)
        }
        
        cardView.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(respondToCardTap))
        )

    }
    
    @objc func respondToCardSwipe(gesture: UIGestureRecognizer) {
        
        if let gesture = gesture as? UISwipeGestureRecognizer{
            switch gesture.direction {
            case .right:
                correctButton(self)
                cardView.backgroundColor = #colorLiteral(red: 0.2058806717, green: 0.7801990509, blue: 0.3488496244, alpha: 1)
                DispatchQueue.main.asyncAfter(deadline: .now()+0.3, execute: {
                    self.cardView.backgroundColor = #colorLiteral(red: 0.4976247549, green: 0.2145499587, blue: 0.842060864, alpha: 1)
                })
            case .left:
                missedButton(self)
                cardView.backgroundColor = #colorLiteral(red: 0.9987934232, green: 0.2306846678, blue: 0.1864726841, alpha: 1)
                DispatchQueue.main.asyncAfter(deadline: .now()+0.3, execute: {
                    self.cardView.backgroundColor = #colorLiteral(red: 0.4976247549, green: 0.2145499587, blue: 0.842060864, alpha: 1)
                })
            case .up:
                skipButton(self)
                cardView.backgroundColor = #colorLiteral(red: 0.006345573347, green: 0.478813827, blue: 0.9984634519, alpha: 1)
                DispatchQueue.main.asyncAfter(deadline: .now()+0.3, execute: {
                    self.cardView.backgroundColor = #colorLiteral(red: 0.4976247549, green: 0.2145499587, blue: 0.842060864, alpha: 1)
                })
            default:
                break
            }
        }
    }
    
    @objc func respondToCardTap(_: UIGestureRecognizer) {
        definitionText.isHidden = !definitionText.isHidden
    }
    
    @IBAction func correctButton(_ sender: Any) {
        if cards.count == 0 {
            showDoneAlert()
        } else if cards.count == 1 {
            if !missedCards.contains(cards[0]) { correct += 1 }
            complete += 1
            updateCounts()

            cards.removeFirst()
            clearCard()
            showDoneAlert()
        } else if cards.count > 1 {
            if !missedCards.contains(cards[0]) { correct += 1 }
            complete += 1
            updateCounts()
            
            cards.removeFirst()
            updateCard()
        }
    }
    
    @IBAction func missedButton(_ sender: Any) {
            if !missedCards.contains(cards[0]) { missedCards.append(cards[0]) }
            updateCounts()
            
            cards.append(cards[0])
            cards.removeFirst()
            updateCard()
    }
    @IBAction func skipButton(_ sender: Any) {
            cards.append(cards[0])
            cards.removeFirst()
            updateCard()
    }
    
    @IBAction func exitButton(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func updateCard() {
        if !cardCleared {
            termText.text = cards[0].term
            definitionText.text = cards[0].definition
            definitionText.isHidden = true
        }
    }
    
    func clearCard() {
        termText.text = ""
        definitionText.text = ""
        cardCleared = true
    }
    
    func updateCounts() {
        missedCountText.text = "\(missedCards.count)"
        correctCountText.text = "\(correct)"
        completedCountText.text = "Completed: \(complete)/\(startCount)"
    }
    
    func showDoneAlert() {
        let alert = UIAlertController(title: "You have studied all of your flashcards!", message: "Correct: \(correct)\nIncorrect \(missedCards.count)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in
            _ = self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true)
    }
    

    func doFormat() {
        cardView.layer.cornerRadius = 12.0
        missedButton.layer.cornerRadius = 6.0
        skipButton.layer.cornerRadius = 6.0
        correctButton.layer.cornerRadius = 6.0
        exitButton.layer.cornerRadius = 6.0
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
